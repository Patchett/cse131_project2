/* File: parser.y
 * --------------
 * Bison input file to generate the parser for the compiler.
 *
 * pp2: your job is to write a parser that will construct the parse tree
 *      and if no parse errors were found, print it.  The parser should
 *      accept the language as described in specification, and as augmented
 *      in the pp2 handout.
 */

%{

/* Just like lex, the text within this first region delimited by %{ and %}
 * is assumed to be C/C++ code and will be copied verbatim to the y.tab.c
 * file ahead of the definitions of the yyparse() function. Add other header
 * file inclusions or C++ variable declarations/prototypes that are needed
 * by your code here.
 */
#include "scanner.h" // for yylex
#include "parser.h"
#include "errors.h"

void yyerror(const char *msg); // standard error-handling routine

%}

/* The section before the first %% is the Definitions section of the yacc
 * input file. Here is where you declare tokens and types, add precedence
 * and associativity options, and so on.
 */

/* yylval
 * ------
 * Here we define the type of the yylval global variable that is used by
 * the scanner to store attibute information about the token just scanned
 * and thus communicate that information to the parser.
 *
 * pp2: You will need to add new fields to this union as you add different
 *      attributes to your non-terminal symbols.
 */

%code requires {
    struct Initialized_Declaration_Struct {
        AssignExpr *assignment;
        VarDecl *declaration;
    };
    struct Switch_Statement_Core_Struct {
        Default *def;
        List<Case*> *case_list;
    };
    struct Statement_List_Struct
    {
      List<VarDecl*> *declarationList;
      List<Stmt*> *statementList;
    };
    struct Rest_Statement_Struct {
        Stmt *tb;
        Stmt *eb;
    };
    struct For_Rest_Statement_Struct {
        Expr *test;
        Expr *step;
    };
}

%union {
    int integerConstant;
    bool boolConstant;
    char *stringConstant;
    float floatConstant;
    char identifier[MaxIdentLen+1]; // +1 for terminating null
    Identifier *id;
    IntConstant *intConstant;
    Decl *decl;
    List<Decl*> *declList;
    VarDecl *variableDecl;
    FnDecl *functionDecl;
    Type *type;
    Stmt *statement;
    Expr *expr;
    IfStmt *ifStatement;
    List<VarDecl*> *variableList;
    void *nothing;
    Operator *op;
    // Switch statement stuff
    LoopStmt *iterationStatement;
    Default *defa;
    SwitchStmt *switchStmt;
    Case *caseStmt;
    List<Case*> *caseStmtList;
    // Structs
    struct Rest_Statement_Struct rest_statement_struct;
    struct Statement_List_Struct statementList;
    struct Switch_Statement_Core_Struct switch_statement_core_struct;
    struct For_Rest_Statement_Struct for_rest_statement_struct;
    Initialized_Declaration_Struct initialized_declaration_struct;
}


/* Tokens
 * ------
 * Here we tell yacc about all the token types that we are using.
 * Bison will assign unique numbers to these and export the #define
 * in the generated y.tab.h header file.
 */
%token   T_Void T_Bool T_Int T_Float
%token   T_LessEqual T_GreaterEqual T_Equal T_NotEqual T_Dims
%token   T_And T_Or
%token   T_While T_For T_If T_Else T_Return T_Break
%token   T_Inc T_Dec T_Switch T_Case T_Default T_Continue
%token   T_Assign_Multiply T_Assign_Divide T_Assign_Add T_Assign_Subtract
%token   T_Vec2 T_Vec3 T_Vec4 T_Mat2 T_Mat3 T_Mat4

%token   <identifier> T_Identifier
%token   <identifier> T_FieldSelect
%token   <integerConstant> T_IntConstant
%token   <floatConstant> T_FloatConstant
%token   <boolConstant> T_BoolConstant

/* Non-terminal types
 * ------------------
 * In order for yacc to assign/access the correct field of $$, $1, we
 * must to declare which field is appropriate for the non-terminal.
 * As an example, this first type declaration establishes that the DeclList
 * non-terminal uses the field named "declList" in the yylval union. This
 * means that when we are setting $$ for a reduction for DeclList ore reading
 * $n which corresponds to a DeclList nonterminal we are accessing the field
 * of the union named "declList" which is of type List<Decl*>.
 * pp2: You'll need to add many of these of your own.
 */
%type <declList>            TranslationUnit
%type <decl>                Decl External_Decl
%type <expr>                Primary_Expr
%type <expr>                Unary_Expr
%type <expr>                Postfix_Expr
%type <expr>                Multiplicative_Expr
%type <id>                  Variable_Identifier
%type <variableList>        Arguments
%type <variableDecl>        Argument
%type <functionDecl>        FnDef
%type <functionDecl>        FnPrototype
%type <variableDecl>        Basic_Var_Decl
%type <type>                Fully_Spec_Type
%type <statement>           CompoundStmtNoScope
%type <statement>           Statement
%type <statement>           Simple_Statement
%type <statement>           Jump_Statement
%type <statementList>       Statement_List
%type <statementList>       Statement_List_Switch
%type <ifStatement>         Select_Statement
%type <expr>                Logical_And_Expr
%type <expr>                Logical_Or_Expr
%type <expr>                Assignment_Expr
%type <expr>                Expr_Statement
%type <expr>                Relational_Expr
%type <expr>                Additive_Expr
%type <expr>                Equality_Expr
%type <expr>                Condition
%type <op>                  Unary_Op
%type <op>                  Assignment_Op
%type <initialized_declaration_struct> Initialized_Declaration
%type <initialized_declaration_struct> Initialized_Var_Declaration
%type <rest_statement_struct>          Select_Rest_Statement
// For/While Loop stuff
%type <iterationStatement>             Iteration_Statement
%type <for_rest_statement_struct>      For_Rest_Statement
//Switch statement stuff
%type <caseStmt>                       Case_Statement
%type <caseStmtList>                   Case_Statement_List
%type <switch_statement_core_struct>   Switch_Statement_Core
%type <intConstant>                    Case_Switch_Label
%type <nothing>                        Default_Switch_Label
%type <defa>                           Default_Statement
%type <switchStmt>                     Switch_Statement
%nonassoc "then"
%nonassoc T_Else

%%
/* Rules
 * -----
 * All productions and actions should be placed between the start and stop
 * %% markers which delimit the Rules section.

 */
Program   :    TranslationUnit      {
                                      @1;
                                      /* pp2: The @1 is needed to convince
                                       * yacc to set up yylloc. You can remove
                                       * it once you have other uses of @n*/
                                      Program *program = new Program($1);
                                      // if no errors, advance to next phase
                                      if (ReportError::NumErrors() == 0)
                                          program->Print(0);
                                    }
                               ;
Variable_Identifier            :    T_Identifier { $$ = new Identifier(@1, $1); }
                               ;
Primary_Expr                   :    Variable_Identifier            { $$ = new VarExpr(@1, $1); }
                               |    T_IntConstant                  { $$ = new IntConstant(@1,$1); }
                               |    T_FloatConstant                { $$ = new FloatConstant(@1,$1); }
                               |    T_BoolConstant                 { $$ = new BoolConstant(@1,$1); }
                               |    '(' Assignment_Expr ')'        { $$ = $2; }
                               ;
Postfix_Expr                   :    Primary_Expr                   { $$ = $1;}
                               |    Postfix_Expr '.' T_FieldSelect { $$ = new FieldAccess($1, new Identifier(@3, $3)); } //*!! May need adjustment*/
                               |    Postfix_Expr T_Inc             { $$ = new PostfixExpr($1, new Operator(@2, "++"));}
                               |    Postfix_Expr T_Dec             { $$ = new PostfixExpr($1, new Operator(@2, "--"));}
                               ;
// Do integer_expression later
// Do function_identifier later
Unary_Expr                     :    Postfix_Expr                   { $$ = $1;}
                               |    T_Dec Unary_Expr               { $$ = new ArithmeticExpr(new Operator(@1,"--"), $2);}
                               |    T_Inc Unary_Expr               { $$ = new ArithmeticExpr(new Operator(@1,"++"), $2);}
                               |    Unary_Op Unary_Expr            { $$ = new ArithmeticExpr($1,$2);}
                               ;
Unary_Op                       :    '-'                            { $$ = new Operator(@1, "-"); }
                               |    '+'                            { $$ = new Operator(@1, "+"); }
                               ;
Multiplicative_Expr            :    Unary_Expr                            { $$ = $1;}
                               |    Multiplicative_Expr '*' Unary_Expr    { $$ = new ArithmeticExpr($1, new Operator(@2,"*"),$3);}
                               |    Multiplicative_Expr '/' Unary_Expr    { $$ = new ArithmeticExpr($1, new Operator(@2,"/"),$3);}
                               ;
Additive_Expr                  :    Multiplicative_Expr { $$ = $1; }
                               |    Additive_Expr '+' Multiplicative_Expr { $$ = new ArithmeticExpr($1, new Operator(@2, "+"),$3);}
                               |    Additive_Expr '-' Multiplicative_Expr { $$ = new ArithmeticExpr($1, new Operator(@2, "-"),$3);}
                               ;
Relational_Expr                :    Additive_Expr                                {$$ = $1;}
                               |    Relational_Expr '<' Additive_Expr            { $$ = new RelationalExpr($1, new Operator(@2, "<"), $3);}
                               |    Relational_Expr '>' Additive_Expr            { $$ = new RelationalExpr($1, new Operator(@2, ">"), $3);}
                               |    Relational_Expr T_LessEqual Additive_Expr    { $$ = new RelationalExpr($1, new Operator(@2, "<="), $3);}
                               |    Relational_Expr T_GreaterEqual Additive_Expr { $$ = new RelationalExpr($1, new Operator(@2, ">="), $3);}
                               ;
Equality_Expr                  :    Relational_Expr                          { $$ = $1;}
                               |    Equality_Expr T_Equal Relational_Expr    { $$ = new EqualityExpr($1, new Operator(@2, "=="), $3);}
                               |    Equality_Expr T_NotEqual Relational_Expr { $$ = new EqualityExpr($1, new Operator(@2, "!="), $3);}
                               ;
Logical_And_Expr               :    Equality_Expr                            { $$ = $1; }
                               |    Logical_And_Expr T_And Equality_Expr     { $$ = new LogicalExpr($1,new Operator(@2, "&&"),$3);}
                               ;
Logical_Or_Expr                :    Logical_And_Expr                         { $$ = $1; }
                               |    Logical_Or_Expr T_Or Equality_Expr       { $$ = new LogicalExpr($1,new Operator(@2, "||"),$3);}
                               ;
Assignment_Expr                :    Logical_Or_Expr                          { $$ = $1; }
                               |    Unary_Expr Assignment_Op Assignment_Expr { $$ = new AssignExpr($1, $2, $3);}
                               ;
Assignment_Op                  :    '='               { $$ = new Operator(@1, "=");}
                               |    T_Assign_Multiply { $$ = new Operator(@1, "*=");}
                               |    T_Assign_Divide   { $$ = new Operator(@1, "/=");}
                               |    T_Assign_Add      { $$ = new Operator(@1, "+=");}
                               |    T_Assign_Subtract { $$ = new Operator(@1, "-=");}
                               ;
Decl                           :    Basic_Var_Decl         { $$ = $1; }
                               |    FnPrototype ';'        { $$ = $1; }
                               ;
Basic_Var_Decl                 :    Fully_Spec_Type T_Identifier ';'  { $$ = new VarDecl(new Identifier(@2, $2), $1); }
                               ;
Initialized_Declaration        :    Fully_Spec_Type T_Identifier '=' Assignment_Expr { $$.declaration = new VarDecl(new Identifier(@2, $2), $1);
                                    $$.assignment = new AssignExpr(new VarExpr(@2, new Identifier(@2, $2)), new Operator(@3, "="), $4); }
                               ;
Initialized_Var_Declaration    :    Initialized_Declaration ';' { $$ = $1; }
                               ;
FnPrototype                    :    Fully_Spec_Type T_Identifier '(' Arguments ')'  { $$ = new FnDecl(new Identifier(@2, $2), $1, $4); }
                               ;
Arguments                      :    Arguments ',' Argument   { ($$ = $1)->Append($3); } //case for multiple arguments
                               |    Arguments Argument       { ($$ = $1)->Append($2); } //case for one argument
                               |                             { $$ = new List<VarDecl*>; } //base case/no arguments
                               ;
Argument                       :    Fully_Spec_Type T_Identifier { $$ = new VarDecl(new Identifier(@2, $2), $1); }
                               |    Fully_Spec_Type              { $$ = new VarDecl(new Identifier(@1, ""), $1); }
                               ;
Fully_Spec_Type                :    T_Void                { $$ = Type::voidType; }
                               |    T_Bool                { $$ = Type::boolType; }
                               |    T_Int                 { $$ = Type::intType;  }
                               |    T_Float               { $$ = Type::floatType; }
                               |    T_Vec2                { $$ = Type::vec2Type; }
                               |    T_Vec3                { $$ = Type::vec3Type; }
                               |    T_Vec4                { $$ = Type::vec4Type; }
                               |    T_Mat2                { $$ = Type::mat2Type; }
                               |    T_Mat3                { $$ = Type::mat3Type; }
                               |    T_Mat4                { $$ = Type::mat4Type; }
                               ;
Iteration_Statement            :    T_While '(' Condition ')' Statement   { $$ = new WhileStmt($3, $5); }
                               |    T_For '(' Expr_Statement For_Rest_Statement ')' Statement { $$ = new ForStmt($3, $4.test, $4.step, $6); }
                               ;
For_Rest_Statement             :    Condition ';'                 { $$.test = $1; $$.step = NULL; }
                               |    Condition ';' Assignment_Expr { $$.test = $1; $$.step = $3;   }
                               ;
Statement                      :    CompoundStmtNoScope   { $$=$1; }
                               |    Simple_Statement      { $$=$1; }
                               ;
Simple_Statement               :    Expr_Statement        { $$=$1; }
                               |    Jump_Statement        { $$=$1; }
                               |    Select_Statement      { $$=$1; }
                               |    Switch_Statement      { $$=$1; }
                               |    Iteration_Statement   { $$=$1; }
                               ;
Jump_Statement                 :    T_Break ';'                  { $$ = new BreakStmt(@1); }
                               |    T_Return ';'                 { $$ = new ReturnStmt(@1, new EmptyExpr()); }
                               |    T_Return Assignment_Expr ';' { $$ = new ReturnStmt(@1, $2); }
                               ;
Expr_Statement                 :    ';'                    { $$ = new EmptyExpr(); }
                               |    Assignment_Expr ';'    { $$ = $1; }
                               ;
Select_Statement               :    T_If '(' Assignment_Expr ')' Select_Rest_Statement { $$ = new IfStmt($3, $5.tb, $5.eb); }
                               ;
Switch_Statement               :    T_Switch '(' Assignment_Expr ')' '{' Switch_Statement_Core '}' { $$ = new SwitchStmt($3, $6.case_list,$6.def);}
                               ;
Switch_Statement_Core          :    Case_Statement_List Default_Statement { $$.def = $2; $$.case_list = $1;}
                               |    Case_Statement_List                   { $$.def = NULL; $$.case_list = $1;}
                               ;
Default_Statement              :    Default_Switch_Label '{' Statement_List_Switch '}' {$$ = new Default($3.statementList);}
                               |    Default_Switch_Label Statement_List_Switch         {$$ = new Default($2.statementList);}
                               ;
Case_Statement                 :    Case_Switch_Label '{' Statement_List_Switch '}'    { $$ = new Case($1, $3.statementList); }
                               |    Case_Switch_Label Statement_List_Switch            { $$ = new Case($1, $2.statementList); }
                               ;
Case_Statement_List            :    Case_Statement_List Case_Statement    { ($$ = $1)->Append($2); }
                               |    Case_Statement                        { ($$ = new List<Case*>())->Append($1); }
                               ;
Case_Switch_Label              :    T_Case T_IntConstant ':'              { $$ = new IntConstant(@2,$2); }
                               ;
Default_Switch_Label           :    T_Default ':' {}
                               ;    // For and While loops
Select_Rest_Statement          :    Statement T_Else Statement    { $$.tb = $1; $$.eb=$3; }
                               |    Statement                     { $$.tb = $1; $$.eb = NULL;}  %prec "then"
                               ;
Condition                      :    Assignment_Expr                                      { $$ = $1; }
                               |    Fully_Spec_Type T_Identifier T_Equal Assignment_Expr { $$ = new AssignExpr(new VarExpr(@2, new Identifier(@2, $2)), new Operator(@3, "=="), $4); }
                               ;
TranslationUnit                :    TranslationUnit External_Decl   { ($$ = $1)->Append($2); }
                               |    External_Decl                   { ($$ = new List<Decl*>)->Append($1); }
                               ;
External_Decl                  :    FnDef         { $$ = $1; }
                               |    Decl          { $$ = $1; }
                               ;
FnDef                          :    FnPrototype CompoundStmtNoScope { ($$ = $1)->SetFunctionBody($2); }
                               ;
CompoundStmtNoScope            :    '{' '}'                 { $$ = new StmtBlock(new List<VarDecl*>, new List<Stmt*>); }
                               |    '{' Statement_List '}'  { $$ = new StmtBlock($2.declarationList, $2.statementList); }
                               ;
Statement_List_Switch          :    Statement_List_Switch Simple_Statement     { $$.statementList->Append($2); }
                               |    Statement_List_Switch Basic_Var_Decl       { $$.declarationList->Append($2); }
                               |    Basic_Var_Decl                             { ($$.declarationList = new List<VarDecl*>())->Append($1); $$.statementList = new List<Stmt*>(); }
                               |    Simple_Statement                           { $$.declarationList = new List<VarDecl*>(); ($$.statementList = new List<Stmt*>())->Append($1); }
                               ;
Statement_List                 :    Statement                                  { ($$.statementList = new List<Stmt*>())->Append($1); $$.declarationList = new List<VarDecl*>(); }
                               |    Statement_List Statement                   { $$ = $1; $$.statementList->Append($2); }
                               |    Basic_Var_Decl                             { ($$.declarationList = new List<VarDecl*>())->Append($1); $$.statementList = new List<Stmt*>(); }
                               |    Statement_List Basic_Var_Decl              { $$ = $1; $$.declarationList->Append($2); }
                               |    Statement_List Initialized_Var_Declaration { $$ = $1; $$.declarationList->Append($2.declaration); $$.statementList->Append($2.assignment); }
                               |    Initialized_Var_Declaration                { ($$.declarationList = new List<VarDecl*>())->Append($1.declaration);
                                                                                 ($$.statementList = new List<Stmt*>())->Append($1.assignment); }
                               ;

%%

/* The closing %% above marks the end of the Rules section and the beginning
 * of the User Subroutines section. All text from here to the end of the
 * file is copied verbatim to the end of the generated y.tab.c file.
 * This section is where you put definitions of helper functions.
 */

/* Function: InitParser
 * --------------------
 * This function will be called before any calls to yyparse().  It is designed
 * to give you an opportunity to do anything that must be done to initialize
 * the parser (set global variables, configure starting state, etc.). One
 * thing it already does for you is assign the value of the global variable
 * yydebug that controls whether yacc prints debugging information about
 * parser actions (shift/reduce) and contents of state stack during parser.
 * If set to false, no information is printed. Setting it to true will give
 * you a running trail that might be helpful when debugging your parser.
 * Please be sure the variable is set to false when submitting your final
 * version.
 */
void InitParser()
{
   PrintDebug("parser", "Initializing parser");
   yydebug = false;
}

// NOTES
// Given scanner.l does not catch all floats. But this is ok https://piazza.com/class/ihrbfc16rae4j0?cid=306

// TODO
// README
// Vector constructors


// Done
// Error checking in scanner.l
// Add additional rules from p0 scanner
// Jmp statements


