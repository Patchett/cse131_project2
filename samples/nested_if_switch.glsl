/* switch statements
   Note: Identifiers may be FieldAccess or VarExpr. Either is acceptable.
*/

void foo() {
  int i;
  int j;

  switch (i) {
    case 1:
      if (j == 1) {
        i = 1;
      } else {
        i = 2;
      }
      i = 2;
    case 2:
      i = 1;
    default:
      if (j == 1) {
        i = 1;
      } else {
        i = 2;
      }
      i = 1;
  }
}
