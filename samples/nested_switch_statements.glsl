/* switch statements
   Note: Identifiers may be FieldAccess or VarExpr. Either is acceptable.
*/

void foo() {
  int i;
  int j;

  switch (i) {
    case 1:
      switch (j) {
        case 1: i = 1;
        case 2: i = 2;
        case 3: i = 3;
        default: i = 4;
      }
    case 2:
      i = 1;
    default:
      if (j == 1) {
        i = 1;
      } else {
        i = 2;
      }
      i = 1;
  }
}
