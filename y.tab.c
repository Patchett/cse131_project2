
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1

/* Using locations.  */
#define YYLSP_NEEDED 1



/* Copy the first part of user declarations.  */

/* Line 189 of yacc.c  */
#line 11 "parser.y"


/* Just like lex, the text within this first region delimited by %{ and %}
 * is assumed to be C/C++ code and will be copied verbatim to the y.tab.c
 * file ahead of the definitions of the yyparse() function. Add other header
 * file inclusions or C++ variable declarations/prototypes that are needed
 * by your code here.
 */
#include "scanner.h" // for yylex
#include "parser.h"
#include "errors.h"

void yyerror(const char *msg); // standard error-handling routine



/* Line 189 of yacc.c  */
#line 90 "y.tab.c"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

/* "%code requires" blocks.  */

/* Line 209 of yacc.c  */
#line 42 "parser.y"

    struct Initialized_Declaration_Struct {
        AssignExpr *assignment;
        VarDecl *declaration;
    };
    struct Switch_Statement_Core_Struct {
        Default *def;
        List<Case*> *case_list;
    };
    struct Statement_List_Struct
    {
      List<VarDecl*> *declarationList;
      List<Stmt*> *statementList;
    };
    struct Rest_Statement_Struct {
        Stmt *tb;
        Stmt *eb;
    };
    struct For_Rest_Statement_Struct {
        Expr *test;
        Expr *step;
    };



/* Line 209 of yacc.c  */
#line 140 "y.tab.c"

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     T_Void = 258,
     T_Bool = 259,
     T_Int = 260,
     T_Float = 261,
     T_LessEqual = 262,
     T_GreaterEqual = 263,
     T_Equal = 264,
     T_NotEqual = 265,
     T_Dims = 266,
     T_And = 267,
     T_Or = 268,
     T_While = 269,
     T_For = 270,
     T_If = 271,
     T_Else = 272,
     T_Return = 273,
     T_Break = 274,
     T_Inc = 275,
     T_Dec = 276,
     T_Switch = 277,
     T_Case = 278,
     T_Default = 279,
     T_Continue = 280,
     T_Assign_Multiply = 281,
     T_Assign_Divide = 282,
     T_Assign_Add = 283,
     T_Assign_Subtract = 284,
     T_Vec2 = 285,
     T_Vec3 = 286,
     T_Vec4 = 287,
     T_Mat2 = 288,
     T_Mat3 = 289,
     T_Mat4 = 290,
     T_Identifier = 291,
     T_FieldSelect = 292,
     T_IntConstant = 293,
     T_FloatConstant = 294,
     T_BoolConstant = 295
   };
#endif
/* Tokens.  */
#define T_Void 258
#define T_Bool 259
#define T_Int 260
#define T_Float 261
#define T_LessEqual 262
#define T_GreaterEqual 263
#define T_Equal 264
#define T_NotEqual 265
#define T_Dims 266
#define T_And 267
#define T_Or 268
#define T_While 269
#define T_For 270
#define T_If 271
#define T_Else 272
#define T_Return 273
#define T_Break 274
#define T_Inc 275
#define T_Dec 276
#define T_Switch 277
#define T_Case 278
#define T_Default 279
#define T_Continue 280
#define T_Assign_Multiply 281
#define T_Assign_Divide 282
#define T_Assign_Add 283
#define T_Assign_Subtract 284
#define T_Vec2 285
#define T_Vec3 286
#define T_Vec4 287
#define T_Mat2 288
#define T_Mat3 289
#define T_Mat4 290
#define T_Identifier 291
#define T_FieldSelect 292
#define T_IntConstant 293
#define T_FloatConstant 294
#define T_BoolConstant 295




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 214 of yacc.c  */
#line 66 "parser.y"

    int integerConstant;
    bool boolConstant;
    char *stringConstant;
    float floatConstant;
    char identifier[MaxIdentLen+1]; // +1 for terminating null
    Identifier *id;
    IntConstant *intConstant;
    Decl *decl;
    List<Decl*> *declList;
    VarDecl *variableDecl;
    FnDecl *functionDecl;
    Type *type;
    Stmt *statement;
    Expr *expr;
    IfStmt *ifStatement;
    List<VarDecl*> *variableList;
    void *nothing;
    Operator *op;
    // Switch statement stuff
    LoopStmt *iterationStatement;
    Default *defa;
    SwitchStmt *switchStmt;
    Case *caseStmt;
    List<Case*> *caseStmtList;
    // Structs
    struct Rest_Statement_Struct rest_statement_struct;
    struct Statement_List_Struct statementList;
    struct Switch_Statement_Core_Struct switch_statement_core_struct;
    struct For_Rest_Statement_Struct for_rest_statement_struct;
    Initialized_Declaration_Struct initialized_declaration_struct;



/* Line 214 of yacc.c  */
#line 272 "y.tab.c"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif


/* Copy the second part of user declarations.  */


/* Line 264 of yacc.c  */
#line 297 "y.tab.c"

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#elif (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
typedef signed char yytype_int8;
#else
typedef short int yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int yyi)
#else
static int
YYID (yyi)
    int yyi;
#endif
{
  return yyi;
}
#endif

#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     ifndef _STDLIB_H
#      define _STDLIB_H 1
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (YYID (0))
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined _STDLIB_H \
       && ! ((defined YYMALLOC || defined malloc) \
	     && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef _STDLIB_H
#    define _STDLIB_H 1
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined _STDLIB_H && (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
	 || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
	     && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
  YYLTYPE yyls_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE) + sizeof (YYLTYPE)) \
      + 2 * YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (YYID (0))
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)				\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack_alloc, Stack, yysize);			\
	Stack = &yyptr->Stack_alloc;					\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (YYID (0))

#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  19
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   564

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  57
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  45
/* YYNRULES -- Number of rules.  */
#define YYNRULES  113
/* YYNRULES -- Number of states.  */
#define YYNSTATES  185

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   296

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
      42,    43,    47,    46,    53,    45,    44,    48,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    56,    52,
      49,    51,    50,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    54,     2,    55,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const yytype_uint16 yyprhs[] =
{
       0,     0,     3,     5,     7,     9,    11,    13,    15,    19,
      21,    25,    28,    31,    33,    36,    39,    42,    44,    46,
      48,    52,    56,    58,    62,    66,    68,    72,    76,    80,
      84,    86,    90,    94,    96,   100,   102,   106,   108,   112,
     114,   116,   118,   120,   122,   124,   127,   131,   136,   139,
     145,   149,   152,   153,   156,   158,   160,   162,   164,   166,
     168,   170,   172,   174,   176,   178,   184,   191,   194,   198,
     200,   202,   204,   206,   208,   210,   212,   215,   218,   222,
     224,   227,   233,   241,   244,   246,   251,   254,   259,   262,
     265,   267,   271,   274,   278,   280,   282,   287,   290,   292,
     294,   296,   299,   302,   306,   309,   312,   314,   316,   318,
     321,   323,   326,   329
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const yytype_int8 yyrhs[] =
{
      58,     0,    -1,    96,    -1,    36,    -1,    59,    -1,    38,
      -1,    39,    -1,    40,    -1,    42,    70,    43,    -1,    60,
      -1,    61,    44,    37,    -1,    61,    20,    -1,    61,    21,
      -1,    61,    -1,    21,    62,    -1,    20,    62,    -1,    63,
      62,    -1,    45,    -1,    46,    -1,    62,    -1,    64,    47,
      62,    -1,    64,    48,    62,    -1,    64,    -1,    65,    46,
      64,    -1,    65,    45,    64,    -1,    65,    -1,    66,    49,
      65,    -1,    66,    50,    65,    -1,    66,     7,    65,    -1,
      66,     8,    65,    -1,    66,    -1,    67,     9,    66,    -1,
      67,    10,    66,    -1,    67,    -1,    68,    12,    67,    -1,
      68,    -1,    69,    13,    67,    -1,    69,    -1,    62,    71,
      70,    -1,    51,    -1,    26,    -1,    27,    -1,    28,    -1,
      29,    -1,    73,    -1,    76,    52,    -1,    79,    36,    52,
      -1,    79,    36,    51,    70,    -1,    74,    52,    -1,    79,
      36,    42,    77,    43,    -1,    77,    53,    78,    -1,    77,
      78,    -1,    -1,    79,    36,    -1,    79,    -1,     3,    -1,
       4,    -1,     5,    -1,     6,    -1,    30,    -1,    31,    -1,
      32,    -1,    33,    -1,    34,    -1,    35,    -1,    14,    42,
      95,    43,    82,    -1,    15,    42,    85,    81,    43,    82,
      -1,    95,    52,    -1,    95,    52,    70,    -1,    99,    -1,
      83,    -1,    85,    -1,    84,    -1,    86,    -1,    87,    -1,
      80,    -1,    19,    52,    -1,    18,    52,    -1,    18,    70,
      52,    -1,    52,    -1,    70,    52,    -1,    16,    42,    70,
      43,    94,    -1,    22,    42,    70,    43,    54,    88,    55,
      -1,    91,    89,    -1,    91,    -1,    93,    54,   100,    55,
      -1,    93,   100,    -1,    92,    54,   100,    55,    -1,    92,
     100,    -1,    91,    90,    -1,    90,    -1,    23,    38,    56,
      -1,    24,    56,    -1,    82,    17,    82,    -1,    82,    -1,
      70,    -1,    79,    36,     9,    70,    -1,    96,    97,    -1,
      97,    -1,    98,    -1,    72,    -1,    76,    99,    -1,    54,
      55,    -1,    54,   101,    55,    -1,   100,    83,    -1,   100,
      73,    -1,    73,    -1,    83,    -1,    82,    -1,   101,    82,
      -1,    73,    -1,   101,    73,    -1,   101,    75,    -1,    75,
      -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   185,   185,   196,   198,   199,   200,   201,   202,   204,
     205,   206,   207,   211,   212,   213,   214,   216,   217,   219,
     220,   221,   223,   224,   225,   227,   228,   229,   230,   231,
     233,   234,   235,   237,   238,   240,   241,   243,   244,   246,
     247,   248,   249,   250,   252,   253,   255,   257,   260,   262,
     264,   265,   266,   268,   269,   271,   272,   273,   274,   275,
     276,   277,   278,   279,   280,   282,   283,   285,   286,   288,
     289,   291,   292,   293,   294,   295,   297,   298,   299,   301,
     302,   304,   306,   308,   309,   311,   312,   314,   315,   317,
     318,   320,   322,   324,   325,   327,   328,   330,   331,   333,
     334,   336,   338,   339,   341,   342,   343,   344,   346,   347,
     348,   349,   350,   351
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "T_Void", "T_Bool", "T_Int", "T_Float",
  "T_LessEqual", "T_GreaterEqual", "T_Equal", "T_NotEqual", "T_Dims",
  "T_And", "T_Or", "T_While", "T_For", "T_If", "T_Else", "T_Return",
  "T_Break", "T_Inc", "T_Dec", "T_Switch", "T_Case", "T_Default",
  "T_Continue", "T_Assign_Multiply", "T_Assign_Divide", "T_Assign_Add",
  "T_Assign_Subtract", "T_Vec2", "T_Vec3", "T_Vec4", "T_Mat2", "T_Mat3",
  "T_Mat4", "T_Identifier", "T_FieldSelect", "T_IntConstant",
  "T_FloatConstant", "T_BoolConstant", "\"then\"", "'('", "')'", "'.'",
  "'-'", "'+'", "'*'", "'/'", "'<'", "'>'", "'='", "';'", "','", "'{'",
  "'}'", "':'", "$accept", "Program", "Variable_Identifier",
  "Primary_Expr", "Postfix_Expr", "Unary_Expr", "Unary_Op",
  "Multiplicative_Expr", "Additive_Expr", "Relational_Expr",
  "Equality_Expr", "Logical_And_Expr", "Logical_Or_Expr",
  "Assignment_Expr", "Assignment_Op", "Decl", "Basic_Var_Decl",
  "Initialized_Declaration", "Initialized_Var_Declaration", "FnPrototype",
  "Arguments", "Argument", "Fully_Spec_Type", "Iteration_Statement",
  "For_Rest_Statement", "Statement", "Simple_Statement", "Jump_Statement",
  "Expr_Statement", "Select_Statement", "Switch_Statement",
  "Switch_Statement_Core", "Default_Statement", "Case_Statement",
  "Case_Statement_List", "Case_Switch_Label", "Default_Switch_Label",
  "Select_Rest_Statement", "Condition", "TranslationUnit", "External_Decl",
  "FnDef", "CompoundStmtNoScope", "Statement_List_Switch",
  "Statement_List", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,    40,    41,    46,    45,    43,    42,    47,    60,
      62,    61,    59,    44,   123,   125,    58
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    57,    58,    59,    60,    60,    60,    60,    60,    61,
      61,    61,    61,    62,    62,    62,    62,    63,    63,    64,
      64,    64,    65,    65,    65,    66,    66,    66,    66,    66,
      67,    67,    67,    68,    68,    69,    69,    70,    70,    71,
      71,    71,    71,    71,    72,    72,    73,    74,    75,    76,
      77,    77,    77,    78,    78,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    80,    80,    81,    81,    82,
      82,    83,    83,    83,    83,    83,    84,    84,    84,    85,
      85,    86,    87,    88,    88,    89,    89,    90,    90,    91,
      91,    92,    93,    94,    94,    95,    95,    96,    96,    97,
      97,    98,    99,    99,   100,   100,   100,   100,   101,   101,
     101,   101,   101,   101
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     1,     1,     1,     1,     1,     3,     1,
       3,     2,     2,     1,     2,     2,     2,     1,     1,     1,
       3,     3,     1,     3,     3,     1,     3,     3,     3,     3,
       1,     3,     3,     1,     3,     1,     3,     1,     3,     1,
       1,     1,     1,     1,     1,     2,     3,     4,     2,     5,
       3,     2,     0,     2,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     5,     6,     2,     3,     1,
       1,     1,     1,     1,     1,     1,     2,     2,     3,     1,
       2,     5,     7,     2,     1,     4,     2,     4,     2,     2,
       1,     3,     2,     3,     1,     1,     4,     2,     1,     1,
       1,     2,     2,     3,     2,     2,     1,     1,     1,     2,
       1,     2,     2,     1
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       0,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,     0,   100,    44,     0,     0,     2,    98,    99,     1,
      45,     0,   101,     0,    97,     0,     0,     0,     0,     0,
       0,     0,     0,     3,     5,     6,     7,     0,    17,    18,
      79,   102,     4,     9,    13,    19,     0,    22,    25,    30,
      33,    35,    37,     0,   110,     0,   113,     0,    75,   108,
      70,    72,    71,    73,    74,    69,     0,    52,    46,     0,
       0,     0,    77,     0,    76,    15,    14,     0,     0,    11,
      12,     0,    40,    41,    42,    43,    39,     0,    16,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    80,    48,     0,   103,   111,   112,   109,     0,    95,
       0,     0,     0,     0,    78,     0,     8,    10,    38,    20,
      21,    19,    24,    23,    28,    29,    26,    27,    31,    32,
      34,    36,     0,    49,     0,    51,    54,     0,     0,     0,
       0,     0,     0,    47,    50,    53,     0,    65,     0,    67,
      94,    81,     0,    96,    66,    68,     0,     0,     0,    90,
      84,     0,    93,     0,    82,     0,    83,    89,     0,     0,
     106,     0,   107,    88,    91,    92,     0,    86,     0,     0,
     105,   104,     0,    87,    85
};

/* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,    11,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    87,    12,   170,    55,    56,    14,
     108,   135,   171,    58,   139,    59,    60,    61,    62,    63,
      64,   158,   166,   159,   160,   161,   168,   151,   111,    16,
      17,    18,    65,   173,    66
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -138
static const yytype_int16 yypact[] =
{
      68,  -138,  -138,  -138,  -138,  -138,  -138,  -138,  -138,  -138,
    -138,    30,  -138,  -138,   -25,     6,    68,  -138,  -138,  -138,
    -138,   165,  -138,   -32,  -138,     2,    26,    27,    76,    -5,
     116,   116,    39,  -138,  -138,  -138,  -138,   116,  -138,  -138,
    -138,  -138,  -138,  -138,   -16,    -3,   116,   -33,   -27,     5,
      31,    70,    71,    35,  -138,    36,  -138,    47,  -138,  -138,
    -138,  -138,  -138,  -138,  -138,  -138,   209,  -138,  -138,   452,
     496,   116,  -138,    37,  -138,  -138,  -138,   116,    48,  -138,
    -138,    53,  -138,  -138,  -138,  -138,  -138,   116,  -138,   116,
     116,   116,   116,   116,   116,   116,   116,   116,   116,   116,
     116,  -138,  -138,    -2,  -138,  -138,  -138,  -138,     3,  -138,
      56,    51,   452,    61,  -138,    62,  -138,  -138,  -138,  -138,
    -138,  -138,   -33,   -33,   -27,   -27,   -27,   -27,     5,     5,
      31,    31,   116,  -138,    68,  -138,    72,    98,   485,    67,
      59,   485,    63,  -138,  -138,  -138,   116,  -138,   485,   116,
     102,  -138,   101,  -138,  -138,  -138,   485,    87,    74,  -138,
      29,   341,  -138,    75,  -138,    77,  -138,  -138,   385,   429,
    -138,    90,  -138,   429,  -138,  -138,   429,   429,   253,    78,
    -138,  -138,   297,  -138,  -138
};

/* YYPGOTO[NTERM-NUM].  */
static const yytype_int16 yypgoto[] =
{
    -138,  -138,  -138,  -138,  -138,    49,  -138,   -29,   -36,   -21,
     -35,  -138,  -138,   -26,  -138,  -138,     0,  -138,    66,  -138,
    -138,    -7,     1,  -138,  -138,   -63,   382,  -138,    64,  -138,
    -138,  -138,  -138,   -10,  -138,  -138,  -138,  -138,    41,  -138,
     135,  -138,   143,  -137,  -138
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -1
static const yytype_uint8 yytable[] =
{
      13,    15,    73,   107,    79,    80,     1,     2,     3,     4,
      67,    78,    93,    94,    89,    90,    13,    15,    91,    92,
      68,    54,    57,    82,    83,    84,    85,    20,    81,    21,
      19,   177,   178,     5,     6,     7,     8,     9,    10,   182,
      97,    98,    23,   109,    69,   113,   133,    74,    86,   132,
      68,   115,   157,   165,    95,    96,   134,   124,   125,   126,
     127,   118,   122,   123,   130,   131,   105,    57,    70,    71,
     110,     1,     2,     3,     4,   147,   128,   129,   150,    75,
      76,    77,    99,   103,   100,   154,   109,   101,   102,   114,
     117,   116,   137,   162,   138,    88,    30,    31,     5,     6,
       7,     8,     9,    10,   141,   142,   143,   146,   145,   136,
     148,   149,    33,   110,    34,    35,    36,   152,    37,   156,
     153,    38,    39,   155,   157,   163,   179,   144,    72,   164,
      68,   174,   106,   175,   112,   136,    30,    31,   119,   120,
     121,   121,   121,   121,   121,   121,   121,   121,   121,   121,
     167,    24,    33,   140,    34,    35,    36,    22,    37,     0,
       0,    38,    39,     0,     0,     0,     0,     0,     1,     2,
       3,     4,     0,   180,     0,     0,     0,   180,   180,    25,
      26,    27,   180,    28,    29,    30,    31,    32,     0,     0,
       0,     0,     0,     0,     0,     5,     6,     7,     8,     9,
      10,    33,     0,    34,    35,    36,     0,    37,     0,     0,
      38,    39,     1,     2,     3,     4,     0,    40,     0,    21,
      41,     0,     0,    25,    26,    27,     0,    28,    29,    30,
      31,    32,     0,     0,     0,     0,     0,     0,     0,     5,
       6,     7,     8,     9,    10,    33,     0,    34,    35,    36,
       0,    37,     0,     0,    38,    39,     1,     2,     3,     4,
       0,    40,     0,    21,   104,     0,     0,    25,    26,    27,
       0,    28,    29,    30,    31,    32,     0,     0,     0,     0,
       0,     0,     0,     5,     6,     7,     8,     9,    10,    33,
       0,    34,    35,    36,     0,    37,     0,     0,    38,    39,
       1,     2,     3,     4,     0,    40,     0,     0,   183,     0,
       0,    25,    26,    27,     0,    28,    29,    30,    31,    32,
       0,     0,     0,     0,     0,     0,     0,     5,     6,     7,
       8,     9,    10,    33,     0,    34,    35,    36,     0,    37,
       0,     0,    38,    39,     1,     2,     3,     4,     0,    40,
       0,     0,   184,     0,     0,    25,    26,    27,     0,    28,
      29,    30,    31,    32,     0,     0,     0,     0,     0,     0,
       0,     5,     6,     7,     8,     9,    10,    33,     0,    34,
      35,    36,     0,    37,     0,     0,    38,    39,     1,     2,
       3,     4,     0,    40,     0,   169,     0,     0,     0,    25,
      26,    27,     0,    28,    29,    30,    31,    32,     0,     0,
       0,     0,     0,     0,     0,     5,     6,     7,     8,     9,
      10,    33,     0,    34,    35,    36,     0,    37,     0,     0,
      38,    39,     1,     2,     3,     4,     0,    40,     0,   176,
       0,     0,     0,    25,    26,    27,     0,    28,    29,    30,
      31,    32,     0,     0,     0,     1,     2,     3,     4,     5,
       6,     7,     8,     9,    10,    33,     0,    34,    35,    36,
       0,    37,    30,    31,    38,    39,     0,     0,     0,     0,
       0,    40,     5,     6,     7,     8,     9,    10,    33,     0,
      34,    35,    36,     0,    37,     0,     0,    38,    39,    25,
      26,    27,     0,    28,    29,    30,    31,    32,     0,     0,
       0,     0,     0,     0,     0,     0,    30,    31,     0,     0,
       0,    33,     0,    34,    35,    36,     0,    37,     0,     0,
      38,    39,    33,     0,    34,    35,    36,    40,    37,    21,
       0,    38,    39,   172,     0,     0,     0,     0,    40,     0,
     172,   172,     0,     0,     0,   181,     0,     0,   172,   181,
     181,     0,     0,     0,   181
};

static const yytype_int16 yycheck[] =
{
       0,     0,    28,    66,    20,    21,     3,     4,     5,     6,
      42,    37,     7,     8,    47,    48,    16,    16,    45,    46,
      52,    21,    21,    26,    27,    28,    29,    52,    44,    54,
       0,   168,   169,    30,    31,    32,    33,    34,    35,   176,
       9,    10,    36,    69,    42,    71,    43,    52,    51,    51,
      52,    77,    23,    24,    49,    50,    53,    93,    94,    95,
      96,    87,    91,    92,    99,   100,    66,    66,    42,    42,
      69,     3,     4,     5,     6,   138,    97,    98,   141,    30,
      31,    42,    12,    36,    13,   148,   112,    52,    52,    52,
      37,    43,    36,   156,    43,    46,    20,    21,    30,    31,
      32,    33,    34,    35,    43,    43,   132,     9,    36,   108,
      43,    52,    36,   112,    38,    39,    40,    54,    42,    17,
     146,    45,    46,   149,    23,    38,    36,   134,    52,    55,
      52,    56,    66,    56,    70,   134,    20,    21,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     160,    16,    36,   112,    38,    39,    40,    14,    42,    -1,
      -1,    45,    46,    -1,    -1,    -1,    -1,    -1,     3,     4,
       5,     6,    -1,   173,    -1,    -1,    -1,   177,   178,    14,
      15,    16,   182,    18,    19,    20,    21,    22,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    30,    31,    32,    33,    34,
      35,    36,    -1,    38,    39,    40,    -1,    42,    -1,    -1,
      45,    46,     3,     4,     5,     6,    -1,    52,    -1,    54,
      55,    -1,    -1,    14,    15,    16,    -1,    18,    19,    20,
      21,    22,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    30,
      31,    32,    33,    34,    35,    36,    -1,    38,    39,    40,
      -1,    42,    -1,    -1,    45,    46,     3,     4,     5,     6,
      -1,    52,    -1,    54,    55,    -1,    -1,    14,    15,    16,
      -1,    18,    19,    20,    21,    22,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    30,    31,    32,    33,    34,    35,    36,
      -1,    38,    39,    40,    -1,    42,    -1,    -1,    45,    46,
       3,     4,     5,     6,    -1,    52,    -1,    -1,    55,    -1,
      -1,    14,    15,    16,    -1,    18,    19,    20,    21,    22,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    30,    31,    32,
      33,    34,    35,    36,    -1,    38,    39,    40,    -1,    42,
      -1,    -1,    45,    46,     3,     4,     5,     6,    -1,    52,
      -1,    -1,    55,    -1,    -1,    14,    15,    16,    -1,    18,
      19,    20,    21,    22,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    30,    31,    32,    33,    34,    35,    36,    -1,    38,
      39,    40,    -1,    42,    -1,    -1,    45,    46,     3,     4,
       5,     6,    -1,    52,    -1,    54,    -1,    -1,    -1,    14,
      15,    16,    -1,    18,    19,    20,    21,    22,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    30,    31,    32,    33,    34,
      35,    36,    -1,    38,    39,    40,    -1,    42,    -1,    -1,
      45,    46,     3,     4,     5,     6,    -1,    52,    -1,    54,
      -1,    -1,    -1,    14,    15,    16,    -1,    18,    19,    20,
      21,    22,    -1,    -1,    -1,     3,     4,     5,     6,    30,
      31,    32,    33,    34,    35,    36,    -1,    38,    39,    40,
      -1,    42,    20,    21,    45,    46,    -1,    -1,    -1,    -1,
      -1,    52,    30,    31,    32,    33,    34,    35,    36,    -1,
      38,    39,    40,    -1,    42,    -1,    -1,    45,    46,    14,
      15,    16,    -1,    18,    19,    20,    21,    22,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    20,    21,    -1,    -1,
      -1,    36,    -1,    38,    39,    40,    -1,    42,    -1,    -1,
      45,    46,    36,    -1,    38,    39,    40,    52,    42,    54,
      -1,    45,    46,   161,    -1,    -1,    -1,    -1,    52,    -1,
     168,   169,    -1,    -1,    -1,   173,    -1,    -1,   176,   177,
     178,    -1,    -1,    -1,   182
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,     3,     4,     5,     6,    30,    31,    32,    33,    34,
      35,    58,    72,    73,    76,    79,    96,    97,    98,     0,
      52,    54,    99,    36,    97,    14,    15,    16,    18,    19,
      20,    21,    22,    36,    38,    39,    40,    42,    45,    46,
      52,    55,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    73,    74,    75,    79,    80,    82,
      83,    84,    85,    86,    87,    99,   101,    42,    52,    42,
      42,    42,    52,    70,    52,    62,    62,    42,    70,    20,
      21,    44,    26,    27,    28,    29,    51,    71,    62,    47,
      48,    45,    46,     7,     8,    49,    50,     9,    10,    12,
      13,    52,    52,    36,    55,    73,    75,    82,    77,    70,
      79,    95,    85,    70,    52,    70,    43,    37,    70,    62,
      62,    62,    64,    64,    65,    65,    65,    65,    66,    66,
      67,    67,    51,    43,    53,    78,    79,    36,    43,    81,
      95,    43,    43,    70,    78,    36,     9,    82,    43,    52,
      82,    94,    54,    70,    82,    70,    17,    23,    88,    90,
      91,    92,    82,    38,    55,    24,    89,    90,    93,    54,
      73,    79,    83,   100,    56,    56,    54,   100,   100,    36,
      73,    83,   100,    55,    55
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK (1);						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (YYID (0))


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))                                                    \
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
	      (Loc).first_line, (Loc).first_column,	\
	      (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			  \
do {									  \
  if (yydebug)								  \
    {									  \
      YYFPRINTF (stderr, "%s ", Title);					  \
      yy_symbol_print (stderr,						  \
		  Type, Value, Location); \
      YYFPRINTF (stderr, "\n");						  \
    }									  \
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
#else
static void
yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    YYLTYPE const * const yylocationp;
#endif
{
  if (!yyvaluep)
    return;
  YYUSE (yylocationp);
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp)
#else
static void
yy_symbol_print (yyoutput, yytype, yyvaluep, yylocationp)
    FILE *yyoutput;
    int yytype;
    YYSTYPE const * const yyvaluep;
    YYLTYPE const * const yylocationp;
#endif
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
#else
static void
yy_stack_print (yybottom, yytop)
    yytype_int16 *yybottom;
    yytype_int16 *yytop;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (YYID (0))


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yy_reduce_print (YYSTYPE *yyvsp, YYLTYPE *yylsp, int yyrule)
#else
static void
yy_reduce_print (yyvsp, yylsp, yyrule)
    YYSTYPE *yyvsp;
    YYLTYPE *yylsp;
    int yyrule;
#endif
{
  int yynrhs = yyr2[yyrule];
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
	     yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(yyvsp[(yyi + 1) - (yynrhs)])
		       , &(yylsp[(yyi + 1) - (yynrhs)])		       );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (yyvsp, yylsp, Rule); \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static YYSIZE_T
yystrlen (const char *yystr)
#else
static YYSIZE_T
yystrlen (yystr)
    const char *yystr;
#endif
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static char *
yystpcpy (char *yydest, const char *yysrc)
#else
static char *
yystpcpy (yydest, yysrc)
    char *yydest;
    const char *yysrc;
#endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into YYRESULT an error message about the unexpected token
   YYCHAR while in state YYSTATE.  Return the number of bytes copied,
   including the terminating null byte.  If YYRESULT is null, do not
   copy anything; just return the number of bytes that would be
   copied.  As a special case, return 0 if an ordinary "syntax error"
   message will do.  Return YYSIZE_MAXIMUM if overflow occurs during
   size calculation.  */
static YYSIZE_T
yysyntax_error (char *yyresult, int yystate, int yychar)
{
  int yyn = yypact[yystate];

  if (! (YYPACT_NINF < yyn && yyn <= YYLAST))
    return 0;
  else
    {
      int yytype = YYTRANSLATE (yychar);
      YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
      YYSIZE_T yysize = yysize0;
      YYSIZE_T yysize1;
      int yysize_overflow = 0;
      enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
      char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
      int yyx;

# if 0
      /* This is so xgettext sees the translatable formats that are
	 constructed on the fly.  */
      YY_("syntax error, unexpected %s");
      YY_("syntax error, unexpected %s, expecting %s");
      YY_("syntax error, unexpected %s, expecting %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s");
      YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
# endif
      char *yyfmt;
      char const *yyf;
      static char const yyunexpected[] = "syntax error, unexpected %s";
      static char const yyexpecting[] = ", expecting %s";
      static char const yyor[] = " or %s";
      char yyformat[sizeof yyunexpected
		    + sizeof yyexpecting - 1
		    + ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
		       * (sizeof yyor - 1))];
      char const *yyprefix = yyexpecting;

      /* Start YYX at -YYN if negative to avoid negative indexes in
	 YYCHECK.  */
      int yyxbegin = yyn < 0 ? -yyn : 0;

      /* Stay within bounds of both yycheck and yytname.  */
      int yychecklim = YYLAST - yyn + 1;
      int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
      int yycount = 1;

      yyarg[0] = yytname[yytype];
      yyfmt = yystpcpy (yyformat, yyunexpected);

      for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	  {
	    if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
	      {
		yycount = 1;
		yysize = yysize0;
		yyformat[sizeof yyunexpected - 1] = '\0';
		break;
	      }
	    yyarg[yycount++] = yytname[yyx];
	    yysize1 = yysize + yytnamerr (0, yytname[yyx]);
	    yysize_overflow |= (yysize1 < yysize);
	    yysize = yysize1;
	    yyfmt = yystpcpy (yyfmt, yyprefix);
	    yyprefix = yyor;
	  }

      yyf = YY_(yyformat);
      yysize1 = yysize + yystrlen (yyf);
      yysize_overflow |= (yysize1 < yysize);
      yysize = yysize1;

      if (yysize_overflow)
	return YYSIZE_MAXIMUM;

      if (yyresult)
	{
	  /* Avoid sprintf, as that infringes on the user's name space.
	     Don't have undefined behavior even if the translation
	     produced a string with the wrong number of "%s"s.  */
	  char *yyp = yyresult;
	  int yyi = 0;
	  while ((*yyp = *yyf) != '\0')
	    {
	      if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		{
		  yyp += yytnamerr (yyp, yyarg[yyi++]);
		  yyf += 2;
		}
	      else
		{
		  yyp++;
		  yyf++;
		}
	    }
	}
      return yysize;
    }
}
#endif /* YYERROR_VERBOSE */


/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp)
#else
static void
yydestruct (yymsg, yytype, yyvaluep, yylocationp)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
    YYLTYPE *yylocationp;
#endif
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/* Prevent warnings from -Wmissing-prototypes.  */
#ifdef YYPARSE_PARAM
#if defined __STDC__ || defined __cplusplus
int yyparse (void *YYPARSE_PARAM);
#else
int yyparse ();
#endif
#else /* ! YYPARSE_PARAM */
#if defined __STDC__ || defined __cplusplus
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */


/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Location data for the lookahead symbol.  */
YYLTYPE yylloc;

/* Number of syntax errors so far.  */
int yynerrs;



/*-------------------------.
| yyparse or yypush_parse.  |
`-------------------------*/

#ifdef YYPARSE_PARAM
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void *YYPARSE_PARAM)
#else
int
yyparse (YYPARSE_PARAM)
    void *YYPARSE_PARAM;
#endif
#else /* ! YYPARSE_PARAM */
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{


    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       `yyss': related to states.
       `yyvs': related to semantic values.
       `yyls': related to locations.

       Refer to the stacks thru separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    /* The location stack.  */
    YYLTYPE yylsa[YYINITDEPTH];
    YYLTYPE *yyls;
    YYLTYPE *yylsp;

    /* The locations where the error started and ended.  */
    YYLTYPE yyerror_range[2];

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;
  YYLTYPE yyloc;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N), yylsp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yytoken = 0;
  yyss = yyssa;
  yyvs = yyvsa;
  yyls = yylsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */
  yyssp = yyss;
  yyvsp = yyvs;
  yylsp = yyls;

#if YYLTYPE_IS_TRIVIAL
  /* Initialize the default location before parsing starts.  */
  yylloc.first_line   = yylloc.last_line   = 1;
  yylloc.first_column = yylloc.last_column = 1;
#endif

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack.  Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	yytype_int16 *yyss1 = yyss;
	YYLTYPE *yyls1 = yyls;

	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),
		    &yyls1, yysize * sizeof (*yylsp),
		    &yystacksize);

	yyls = yyls1;
	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	yytype_int16 *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss_alloc, yyss);
	YYSTACK_RELOCATE (yyvs_alloc, yyvs);
	YYSTACK_RELOCATE (yyls_alloc, yyls);
#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;
      yylsp = yyls + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  *++yyvsp = yylval;
  *++yylsp = yylloc;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];

  /* Default location.  */
  YYLLOC_DEFAULT (yyloc, (yylsp - yylen), yylen);
  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:

/* Line 1455 of yacc.c  */
#line 185 "parser.y"
    {
                                      (yylsp[(1) - (1)]);
                                      /* pp2: The @1 is needed to convince
                                       * yacc to set up yylloc. You can remove
                                       * it once you have other uses of @n*/
                                      Program *program = new Program((yyvsp[(1) - (1)].declList));
                                      // if no errors, advance to next phase
                                      if (ReportError::NumErrors() == 0)
                                          program->Print(0);
                                    }
    break;

  case 3:

/* Line 1455 of yacc.c  */
#line 196 "parser.y"
    { (yyval.id) = new Identifier((yylsp[(1) - (1)]), (yyvsp[(1) - (1)].identifier)); }
    break;

  case 4:

/* Line 1455 of yacc.c  */
#line 198 "parser.y"
    { (yyval.expr) = new VarExpr((yylsp[(1) - (1)]), (yyvsp[(1) - (1)].id)); }
    break;

  case 5:

/* Line 1455 of yacc.c  */
#line 199 "parser.y"
    { (yyval.expr) = new IntConstant((yylsp[(1) - (1)]),(yyvsp[(1) - (1)].integerConstant)); }
    break;

  case 6:

/* Line 1455 of yacc.c  */
#line 200 "parser.y"
    { (yyval.expr) = new FloatConstant((yylsp[(1) - (1)]),(yyvsp[(1) - (1)].floatConstant)); }
    break;

  case 7:

/* Line 1455 of yacc.c  */
#line 201 "parser.y"
    { (yyval.expr) = new BoolConstant((yylsp[(1) - (1)]),(yyvsp[(1) - (1)].boolConstant)); }
    break;

  case 8:

/* Line 1455 of yacc.c  */
#line 202 "parser.y"
    { (yyval.expr) = (yyvsp[(2) - (3)].expr); }
    break;

  case 9:

/* Line 1455 of yacc.c  */
#line 204 "parser.y"
    { (yyval.expr) = (yyvsp[(1) - (1)].expr);}
    break;

  case 10:

/* Line 1455 of yacc.c  */
#line 205 "parser.y"
    { (yyval.expr) = new FieldAccess((yyvsp[(1) - (3)].expr), new Identifier((yylsp[(3) - (3)]), (yyvsp[(3) - (3)].identifier))); }
    break;

  case 11:

/* Line 1455 of yacc.c  */
#line 206 "parser.y"
    { (yyval.expr) = new PostfixExpr((yyvsp[(1) - (2)].expr), new Operator((yylsp[(2) - (2)]), "++"));}
    break;

  case 12:

/* Line 1455 of yacc.c  */
#line 207 "parser.y"
    { (yyval.expr) = new PostfixExpr((yyvsp[(1) - (2)].expr), new Operator((yylsp[(2) - (2)]), "--"));}
    break;

  case 13:

/* Line 1455 of yacc.c  */
#line 211 "parser.y"
    { (yyval.expr) = (yyvsp[(1) - (1)].expr);}
    break;

  case 14:

/* Line 1455 of yacc.c  */
#line 212 "parser.y"
    { (yyval.expr) = new ArithmeticExpr(new Operator((yylsp[(1) - (2)]),"--"), (yyvsp[(2) - (2)].expr));}
    break;

  case 15:

/* Line 1455 of yacc.c  */
#line 213 "parser.y"
    { (yyval.expr) = new ArithmeticExpr(new Operator((yylsp[(1) - (2)]),"++"), (yyvsp[(2) - (2)].expr));}
    break;

  case 16:

/* Line 1455 of yacc.c  */
#line 214 "parser.y"
    { (yyval.expr) = new ArithmeticExpr((yyvsp[(1) - (2)].op),(yyvsp[(2) - (2)].expr));}
    break;

  case 17:

/* Line 1455 of yacc.c  */
#line 216 "parser.y"
    { (yyval.op) = new Operator((yylsp[(1) - (1)]), "-"); }
    break;

  case 18:

/* Line 1455 of yacc.c  */
#line 217 "parser.y"
    { (yyval.op) = new Operator((yylsp[(1) - (1)]), "+"); }
    break;

  case 19:

/* Line 1455 of yacc.c  */
#line 219 "parser.y"
    { (yyval.expr) = (yyvsp[(1) - (1)].expr);}
    break;

  case 20:

/* Line 1455 of yacc.c  */
#line 220 "parser.y"
    { (yyval.expr) = new ArithmeticExpr((yyvsp[(1) - (3)].expr), new Operator((yylsp[(2) - (3)]),"*"),(yyvsp[(3) - (3)].expr));}
    break;

  case 21:

/* Line 1455 of yacc.c  */
#line 221 "parser.y"
    { (yyval.expr) = new ArithmeticExpr((yyvsp[(1) - (3)].expr), new Operator((yylsp[(2) - (3)]),"/"),(yyvsp[(3) - (3)].expr));}
    break;

  case 22:

/* Line 1455 of yacc.c  */
#line 223 "parser.y"
    { (yyval.expr) = (yyvsp[(1) - (1)].expr); }
    break;

  case 23:

/* Line 1455 of yacc.c  */
#line 224 "parser.y"
    { (yyval.expr) = new ArithmeticExpr((yyvsp[(1) - (3)].expr), new Operator((yylsp[(2) - (3)]), "+"),(yyvsp[(3) - (3)].expr));}
    break;

  case 24:

/* Line 1455 of yacc.c  */
#line 225 "parser.y"
    { (yyval.expr) = new ArithmeticExpr((yyvsp[(1) - (3)].expr), new Operator((yylsp[(2) - (3)]), "-"),(yyvsp[(3) - (3)].expr));}
    break;

  case 25:

/* Line 1455 of yacc.c  */
#line 227 "parser.y"
    {(yyval.expr) = (yyvsp[(1) - (1)].expr);}
    break;

  case 26:

/* Line 1455 of yacc.c  */
#line 228 "parser.y"
    { (yyval.expr) = new RelationalExpr((yyvsp[(1) - (3)].expr), new Operator((yylsp[(2) - (3)]), "<"), (yyvsp[(3) - (3)].expr));}
    break;

  case 27:

/* Line 1455 of yacc.c  */
#line 229 "parser.y"
    { (yyval.expr) = new RelationalExpr((yyvsp[(1) - (3)].expr), new Operator((yylsp[(2) - (3)]), ">"), (yyvsp[(3) - (3)].expr));}
    break;

  case 28:

/* Line 1455 of yacc.c  */
#line 230 "parser.y"
    { (yyval.expr) = new RelationalExpr((yyvsp[(1) - (3)].expr), new Operator((yylsp[(2) - (3)]), "<="), (yyvsp[(3) - (3)].expr));}
    break;

  case 29:

/* Line 1455 of yacc.c  */
#line 231 "parser.y"
    { (yyval.expr) = new RelationalExpr((yyvsp[(1) - (3)].expr), new Operator((yylsp[(2) - (3)]), ">="), (yyvsp[(3) - (3)].expr));}
    break;

  case 30:

/* Line 1455 of yacc.c  */
#line 233 "parser.y"
    { (yyval.expr) = (yyvsp[(1) - (1)].expr);}
    break;

  case 31:

/* Line 1455 of yacc.c  */
#line 234 "parser.y"
    { (yyval.expr) = new EqualityExpr((yyvsp[(1) - (3)].expr), new Operator((yylsp[(2) - (3)]), "=="), (yyvsp[(3) - (3)].expr));}
    break;

  case 32:

/* Line 1455 of yacc.c  */
#line 235 "parser.y"
    { (yyval.expr) = new EqualityExpr((yyvsp[(1) - (3)].expr), new Operator((yylsp[(2) - (3)]), "!="), (yyvsp[(3) - (3)].expr));}
    break;

  case 33:

/* Line 1455 of yacc.c  */
#line 237 "parser.y"
    { (yyval.expr) = (yyvsp[(1) - (1)].expr); }
    break;

  case 34:

/* Line 1455 of yacc.c  */
#line 238 "parser.y"
    { (yyval.expr) = new LogicalExpr((yyvsp[(1) - (3)].expr),new Operator((yylsp[(2) - (3)]), "&&"),(yyvsp[(3) - (3)].expr));}
    break;

  case 35:

/* Line 1455 of yacc.c  */
#line 240 "parser.y"
    { (yyval.expr) = (yyvsp[(1) - (1)].expr); }
    break;

  case 36:

/* Line 1455 of yacc.c  */
#line 241 "parser.y"
    { (yyval.expr) = new LogicalExpr((yyvsp[(1) - (3)].expr),new Operator((yylsp[(2) - (3)]), "||"),(yyvsp[(3) - (3)].expr));}
    break;

  case 37:

/* Line 1455 of yacc.c  */
#line 243 "parser.y"
    { (yyval.expr) = (yyvsp[(1) - (1)].expr); }
    break;

  case 38:

/* Line 1455 of yacc.c  */
#line 244 "parser.y"
    { (yyval.expr) = new AssignExpr((yyvsp[(1) - (3)].expr), (yyvsp[(2) - (3)].op), (yyvsp[(3) - (3)].expr));}
    break;

  case 39:

/* Line 1455 of yacc.c  */
#line 246 "parser.y"
    { (yyval.op) = new Operator((yylsp[(1) - (1)]), "=");}
    break;

  case 40:

/* Line 1455 of yacc.c  */
#line 247 "parser.y"
    { (yyval.op) = new Operator((yylsp[(1) - (1)]), "*=");}
    break;

  case 41:

/* Line 1455 of yacc.c  */
#line 248 "parser.y"
    { (yyval.op) = new Operator((yylsp[(1) - (1)]), "/=");}
    break;

  case 42:

/* Line 1455 of yacc.c  */
#line 249 "parser.y"
    { (yyval.op) = new Operator((yylsp[(1) - (1)]), "+=");}
    break;

  case 43:

/* Line 1455 of yacc.c  */
#line 250 "parser.y"
    { (yyval.op) = new Operator((yylsp[(1) - (1)]), "-=");}
    break;

  case 44:

/* Line 1455 of yacc.c  */
#line 252 "parser.y"
    { (yyval.decl) = (yyvsp[(1) - (1)].variableDecl); }
    break;

  case 45:

/* Line 1455 of yacc.c  */
#line 253 "parser.y"
    { (yyval.decl) = (yyvsp[(1) - (2)].functionDecl); }
    break;

  case 46:

/* Line 1455 of yacc.c  */
#line 255 "parser.y"
    { (yyval.variableDecl) = new VarDecl(new Identifier((yylsp[(2) - (3)]), (yyvsp[(2) - (3)].identifier)), (yyvsp[(1) - (3)].type)); }
    break;

  case 47:

/* Line 1455 of yacc.c  */
#line 257 "parser.y"
    { (yyval.initialized_declaration_struct).declaration = new VarDecl(new Identifier((yylsp[(2) - (4)]), (yyvsp[(2) - (4)].identifier)), (yyvsp[(1) - (4)].type));
                                    (yyval.initialized_declaration_struct).assignment = new AssignExpr(new VarExpr((yylsp[(2) - (4)]), new Identifier((yylsp[(2) - (4)]), (yyvsp[(2) - (4)].identifier))), new Operator((yylsp[(3) - (4)]), "="), (yyvsp[(4) - (4)].expr)); }
    break;

  case 48:

/* Line 1455 of yacc.c  */
#line 260 "parser.y"
    { (yyval.initialized_declaration_struct) = (yyvsp[(1) - (2)].initialized_declaration_struct); }
    break;

  case 49:

/* Line 1455 of yacc.c  */
#line 262 "parser.y"
    { (yyval.functionDecl) = new FnDecl(new Identifier((yylsp[(2) - (5)]), (yyvsp[(2) - (5)].identifier)), (yyvsp[(1) - (5)].type), (yyvsp[(4) - (5)].variableList)); }
    break;

  case 50:

/* Line 1455 of yacc.c  */
#line 264 "parser.y"
    { ((yyval.variableList) = (yyvsp[(1) - (3)].variableList))->Append((yyvsp[(3) - (3)].variableDecl)); }
    break;

  case 51:

/* Line 1455 of yacc.c  */
#line 265 "parser.y"
    { ((yyval.variableList) = (yyvsp[(1) - (2)].variableList))->Append((yyvsp[(2) - (2)].variableDecl)); }
    break;

  case 52:

/* Line 1455 of yacc.c  */
#line 266 "parser.y"
    { (yyval.variableList) = new List<VarDecl*>; }
    break;

  case 53:

/* Line 1455 of yacc.c  */
#line 268 "parser.y"
    { (yyval.variableDecl) = new VarDecl(new Identifier((yylsp[(2) - (2)]), (yyvsp[(2) - (2)].identifier)), (yyvsp[(1) - (2)].type)); }
    break;

  case 54:

/* Line 1455 of yacc.c  */
#line 269 "parser.y"
    { (yyval.variableDecl) = new VarDecl(new Identifier((yylsp[(1) - (1)]), ""), (yyvsp[(1) - (1)].type)); }
    break;

  case 55:

/* Line 1455 of yacc.c  */
#line 271 "parser.y"
    { (yyval.type) = Type::voidType; }
    break;

  case 56:

/* Line 1455 of yacc.c  */
#line 272 "parser.y"
    { (yyval.type) = Type::boolType; }
    break;

  case 57:

/* Line 1455 of yacc.c  */
#line 273 "parser.y"
    { (yyval.type) = Type::intType;  }
    break;

  case 58:

/* Line 1455 of yacc.c  */
#line 274 "parser.y"
    { (yyval.type) = Type::floatType; }
    break;

  case 59:

/* Line 1455 of yacc.c  */
#line 275 "parser.y"
    { (yyval.type) = Type::vec2Type; }
    break;

  case 60:

/* Line 1455 of yacc.c  */
#line 276 "parser.y"
    { (yyval.type) = Type::vec3Type; }
    break;

  case 61:

/* Line 1455 of yacc.c  */
#line 277 "parser.y"
    { (yyval.type) = Type::vec4Type; }
    break;

  case 62:

/* Line 1455 of yacc.c  */
#line 278 "parser.y"
    { (yyval.type) = Type::mat2Type; }
    break;

  case 63:

/* Line 1455 of yacc.c  */
#line 279 "parser.y"
    { (yyval.type) = Type::mat3Type; }
    break;

  case 64:

/* Line 1455 of yacc.c  */
#line 280 "parser.y"
    { (yyval.type) = Type::mat4Type; }
    break;

  case 65:

/* Line 1455 of yacc.c  */
#line 282 "parser.y"
    { (yyval.iterationStatement) = new WhileStmt((yyvsp[(3) - (5)].expr), (yyvsp[(5) - (5)].statement)); }
    break;

  case 66:

/* Line 1455 of yacc.c  */
#line 283 "parser.y"
    { (yyval.iterationStatement) = new ForStmt((yyvsp[(3) - (6)].expr), (yyvsp[(4) - (6)].for_rest_statement_struct).test, (yyvsp[(4) - (6)].for_rest_statement_struct).step, (yyvsp[(6) - (6)].statement)); }
    break;

  case 67:

/* Line 1455 of yacc.c  */
#line 285 "parser.y"
    { (yyval.for_rest_statement_struct).test = (yyvsp[(1) - (2)].expr); (yyval.for_rest_statement_struct).step = NULL; }
    break;

  case 68:

/* Line 1455 of yacc.c  */
#line 286 "parser.y"
    { (yyval.for_rest_statement_struct).test = (yyvsp[(1) - (3)].expr); (yyval.for_rest_statement_struct).step = (yyvsp[(3) - (3)].expr);   }
    break;

  case 69:

/* Line 1455 of yacc.c  */
#line 288 "parser.y"
    { (yyval.statement)=(yyvsp[(1) - (1)].statement); }
    break;

  case 70:

/* Line 1455 of yacc.c  */
#line 289 "parser.y"
    { (yyval.statement)=(yyvsp[(1) - (1)].statement); }
    break;

  case 71:

/* Line 1455 of yacc.c  */
#line 291 "parser.y"
    { (yyval.statement)=(yyvsp[(1) - (1)].expr); }
    break;

  case 72:

/* Line 1455 of yacc.c  */
#line 292 "parser.y"
    { (yyval.statement)=(yyvsp[(1) - (1)].statement); }
    break;

  case 73:

/* Line 1455 of yacc.c  */
#line 293 "parser.y"
    { (yyval.statement)=(yyvsp[(1) - (1)].ifStatement); }
    break;

  case 74:

/* Line 1455 of yacc.c  */
#line 294 "parser.y"
    { (yyval.statement)=(yyvsp[(1) - (1)].switchStmt); }
    break;

  case 75:

/* Line 1455 of yacc.c  */
#line 295 "parser.y"
    { (yyval.statement)=(yyvsp[(1) - (1)].iterationStatement); }
    break;

  case 76:

/* Line 1455 of yacc.c  */
#line 297 "parser.y"
    { (yyval.statement) = new BreakStmt((yylsp[(1) - (2)])); }
    break;

  case 77:

/* Line 1455 of yacc.c  */
#line 298 "parser.y"
    { (yyval.statement) = new ReturnStmt((yylsp[(1) - (2)]), new EmptyExpr()); }
    break;

  case 78:

/* Line 1455 of yacc.c  */
#line 299 "parser.y"
    { (yyval.statement) = new ReturnStmt((yylsp[(1) - (3)]), (yyvsp[(2) - (3)].expr)); }
    break;

  case 79:

/* Line 1455 of yacc.c  */
#line 301 "parser.y"
    { (yyval.expr) = new EmptyExpr(); }
    break;

  case 80:

/* Line 1455 of yacc.c  */
#line 302 "parser.y"
    { (yyval.expr) = (yyvsp[(1) - (2)].expr); }
    break;

  case 81:

/* Line 1455 of yacc.c  */
#line 304 "parser.y"
    { (yyval.ifStatement) = new IfStmt((yyvsp[(3) - (5)].expr), (yyvsp[(5) - (5)].rest_statement_struct).tb, (yyvsp[(5) - (5)].rest_statement_struct).eb); }
    break;

  case 82:

/* Line 1455 of yacc.c  */
#line 306 "parser.y"
    { (yyval.switchStmt) = new SwitchStmt((yyvsp[(3) - (7)].expr), (yyvsp[(6) - (7)].switch_statement_core_struct).case_list,(yyvsp[(6) - (7)].switch_statement_core_struct).def);}
    break;

  case 83:

/* Line 1455 of yacc.c  */
#line 308 "parser.y"
    { (yyval.switch_statement_core_struct).def = (yyvsp[(2) - (2)].defa); (yyval.switch_statement_core_struct).case_list = (yyvsp[(1) - (2)].caseStmtList);}
    break;

  case 84:

/* Line 1455 of yacc.c  */
#line 309 "parser.y"
    { (yyval.switch_statement_core_struct).def = NULL; (yyval.switch_statement_core_struct).case_list = (yyvsp[(1) - (1)].caseStmtList);}
    break;

  case 85:

/* Line 1455 of yacc.c  */
#line 311 "parser.y"
    {(yyval.defa) = new Default((yyvsp[(3) - (4)].statementList).statementList);}
    break;

  case 86:

/* Line 1455 of yacc.c  */
#line 312 "parser.y"
    {(yyval.defa) = new Default((yyvsp[(2) - (2)].statementList).statementList);}
    break;

  case 87:

/* Line 1455 of yacc.c  */
#line 314 "parser.y"
    { (yyval.caseStmt) = new Case((yyvsp[(1) - (4)].intConstant), (yyvsp[(3) - (4)].statementList).statementList); }
    break;

  case 88:

/* Line 1455 of yacc.c  */
#line 315 "parser.y"
    { (yyval.caseStmt) = new Case((yyvsp[(1) - (2)].intConstant), (yyvsp[(2) - (2)].statementList).statementList); }
    break;

  case 89:

/* Line 1455 of yacc.c  */
#line 317 "parser.y"
    { ((yyval.caseStmtList) = (yyvsp[(1) - (2)].caseStmtList))->Append((yyvsp[(2) - (2)].caseStmt)); }
    break;

  case 90:

/* Line 1455 of yacc.c  */
#line 318 "parser.y"
    { ((yyval.caseStmtList) = new List<Case*>())->Append((yyvsp[(1) - (1)].caseStmt)); }
    break;

  case 91:

/* Line 1455 of yacc.c  */
#line 320 "parser.y"
    { (yyval.intConstant) = new IntConstant((yylsp[(2) - (3)]),(yyvsp[(2) - (3)].integerConstant)); }
    break;

  case 92:

/* Line 1455 of yacc.c  */
#line 322 "parser.y"
    {}
    break;

  case 93:

/* Line 1455 of yacc.c  */
#line 324 "parser.y"
    { (yyval.rest_statement_struct).tb = (yyvsp[(1) - (3)].statement); (yyval.rest_statement_struct).eb=(yyvsp[(3) - (3)].statement); }
    break;

  case 94:

/* Line 1455 of yacc.c  */
#line 325 "parser.y"
    { (yyval.rest_statement_struct).tb = (yyvsp[(1) - (1)].statement); (yyval.rest_statement_struct).eb = NULL;}
    break;

  case 95:

/* Line 1455 of yacc.c  */
#line 327 "parser.y"
    { (yyval.expr) = (yyvsp[(1) - (1)].expr); }
    break;

  case 96:

/* Line 1455 of yacc.c  */
#line 328 "parser.y"
    { (yyval.expr) = new AssignExpr(new VarExpr((yylsp[(2) - (4)]), new Identifier((yylsp[(2) - (4)]), (yyvsp[(2) - (4)].identifier))), new Operator((yylsp[(3) - (4)]), "=="), (yyvsp[(4) - (4)].expr)); }
    break;

  case 97:

/* Line 1455 of yacc.c  */
#line 330 "parser.y"
    { ((yyval.declList) = (yyvsp[(1) - (2)].declList))->Append((yyvsp[(2) - (2)].decl)); }
    break;

  case 98:

/* Line 1455 of yacc.c  */
#line 331 "parser.y"
    { ((yyval.declList) = new List<Decl*>)->Append((yyvsp[(1) - (1)].decl)); }
    break;

  case 99:

/* Line 1455 of yacc.c  */
#line 333 "parser.y"
    { (yyval.decl) = (yyvsp[(1) - (1)].functionDecl); }
    break;

  case 100:

/* Line 1455 of yacc.c  */
#line 334 "parser.y"
    { (yyval.decl) = (yyvsp[(1) - (1)].decl); }
    break;

  case 101:

/* Line 1455 of yacc.c  */
#line 336 "parser.y"
    { ((yyval.functionDecl) = (yyvsp[(1) - (2)].functionDecl))->SetFunctionBody((yyvsp[(2) - (2)].statement)); }
    break;

  case 102:

/* Line 1455 of yacc.c  */
#line 338 "parser.y"
    { (yyval.statement) = new StmtBlock(new List<VarDecl*>, new List<Stmt*>); }
    break;

  case 103:

/* Line 1455 of yacc.c  */
#line 339 "parser.y"
    { (yyval.statement) = new StmtBlock((yyvsp[(2) - (3)].statementList).declarationList, (yyvsp[(2) - (3)].statementList).statementList); }
    break;

  case 104:

/* Line 1455 of yacc.c  */
#line 341 "parser.y"
    { (yyval.statementList).statementList->Append((yyvsp[(2) - (2)].statement)); }
    break;

  case 105:

/* Line 1455 of yacc.c  */
#line 342 "parser.y"
    { (yyval.statementList).declarationList->Append((yyvsp[(2) - (2)].variableDecl)); }
    break;

  case 106:

/* Line 1455 of yacc.c  */
#line 343 "parser.y"
    { ((yyval.statementList).declarationList = new List<VarDecl*>())->Append((yyvsp[(1) - (1)].variableDecl)); (yyval.statementList).statementList = new List<Stmt*>(); }
    break;

  case 107:

/* Line 1455 of yacc.c  */
#line 344 "parser.y"
    { (yyval.statementList).declarationList = new List<VarDecl*>(); ((yyval.statementList).statementList = new List<Stmt*>())->Append((yyvsp[(1) - (1)].statement)); }
    break;

  case 108:

/* Line 1455 of yacc.c  */
#line 346 "parser.y"
    { ((yyval.statementList).statementList = new List<Stmt*>())->Append((yyvsp[(1) - (1)].statement)); (yyval.statementList).declarationList = new List<VarDecl*>(); }
    break;

  case 109:

/* Line 1455 of yacc.c  */
#line 347 "parser.y"
    { (yyval.statementList) = (yyvsp[(1) - (2)].statementList); (yyval.statementList).statementList->Append((yyvsp[(2) - (2)].statement)); }
    break;

  case 110:

/* Line 1455 of yacc.c  */
#line 348 "parser.y"
    { ((yyval.statementList).declarationList = new List<VarDecl*>())->Append((yyvsp[(1) - (1)].variableDecl)); (yyval.statementList).statementList = new List<Stmt*>(); }
    break;

  case 111:

/* Line 1455 of yacc.c  */
#line 349 "parser.y"
    { (yyval.statementList) = (yyvsp[(1) - (2)].statementList); (yyval.statementList).declarationList->Append((yyvsp[(2) - (2)].variableDecl)); }
    break;

  case 112:

/* Line 1455 of yacc.c  */
#line 350 "parser.y"
    { (yyval.statementList) = (yyvsp[(1) - (2)].statementList); (yyval.statementList).declarationList->Append((yyvsp[(2) - (2)].initialized_declaration_struct).declaration); (yyval.statementList).statementList->Append((yyvsp[(2) - (2)].initialized_declaration_struct).assignment); }
    break;

  case 113:

/* Line 1455 of yacc.c  */
#line 351 "parser.y"
    { ((yyval.statementList).declarationList = new List<VarDecl*>())->Append((yyvsp[(1) - (1)].initialized_declaration_struct).declaration);
                                                                                 ((yyval.statementList).statementList = new List<Stmt*>())->Append((yyvsp[(1) - (1)].initialized_declaration_struct).assignment); }
    break;



/* Line 1455 of yacc.c  */
#line 2587 "y.tab.c"
      default: break;
    }
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;
  *++yylsp = yyloc;

  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
      {
	YYSIZE_T yysize = yysyntax_error (0, yystate, yychar);
	if (yymsg_alloc < yysize && yymsg_alloc < YYSTACK_ALLOC_MAXIMUM)
	  {
	    YYSIZE_T yyalloc = 2 * yysize;
	    if (! (yysize <= yyalloc && yyalloc <= YYSTACK_ALLOC_MAXIMUM))
	      yyalloc = YYSTACK_ALLOC_MAXIMUM;
	    if (yymsg != yymsgbuf)
	      YYSTACK_FREE (yymsg);
	    yymsg = (char *) YYSTACK_ALLOC (yyalloc);
	    if (yymsg)
	      yymsg_alloc = yyalloc;
	    else
	      {
		yymsg = yymsgbuf;
		yymsg_alloc = sizeof yymsgbuf;
	      }
	  }

	if (0 < yysize && yysize <= yymsg_alloc)
	  {
	    (void) yysyntax_error (yymsg, yystate, yychar);
	    yyerror (yymsg);
	  }
	else
	  {
	    yyerror (YY_("syntax error"));
	    if (yysize != 0)
	      goto yyexhaustedlab;
	  }
      }
#endif
    }

  yyerror_range[0] = yylloc;

  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
	{
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
	}
      else
	{
	  yydestruct ("Error: discarding",
		      yytoken, &yylval, &yylloc);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  yyerror_range[0] = yylsp[1-yylen];
  /* Do not reclaim the symbols of the rule which action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      yyerror_range[0] = *yylsp;
      yydestruct ("Error: popping",
		  yystos[yystate], yyvsp, yylsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  *++yyvsp = yylval;

  yyerror_range[1] = yylloc;
  /* Using YYLLOC is tempting, but would change the location of
     the lookahead.  YYLOC is available though.  */
  YYLLOC_DEFAULT (yyloc, (yyerror_range - 1), 2);
  *++yylsp = yyloc;

  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined(yyoverflow) || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval, &yylloc);
  /* Do not reclaim the symbols of the rule which action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp, yylsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  /* Make sure YYID is used.  */
  return YYID (yyresult);
}



/* Line 1675 of yacc.c  */
#line 355 "parser.y"


/* The closing %% above marks the end of the Rules section and the beginning
 * of the User Subroutines section. All text from here to the end of the
 * file is copied verbatim to the end of the generated y.tab.c file.
 * This section is where you put definitions of helper functions.
 */

/* Function: InitParser
 * --------------------
 * This function will be called before any calls to yyparse().  It is designed
 * to give you an opportunity to do anything that must be done to initialize
 * the parser (set global variables, configure starting state, etc.). One
 * thing it already does for you is assign the value of the global variable
 * yydebug that controls whether yacc prints debugging information about
 * parser actions (shift/reduce) and contents of state stack during parser.
 * If set to false, no information is printed. Setting it to true will give
 * you a running trail that might be helpful when debugging your parser.
 * Please be sure the variable is set to false when submitting your final
 * version.
 */
void InitParser()
{
   PrintDebug("parser", "Initializing parser");
   yydebug = false;
}

// NOTES
// Given scanner.l does not catch all floats. But this is ok https://piazza.com/class/ihrbfc16rae4j0?cid=306

// TODO
// README
// Vector constructors


// Done
// Error checking in scanner.l
// Add additional rules from p0 scanner
// Jmp statements



