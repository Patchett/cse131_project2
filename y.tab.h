
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison's Yacc-like parsers in C
   
      Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005, 2006
   Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* "%code requires" blocks.  */

/* Line 1676 of yacc.c  */
#line 42 "parser.y"

    struct Initialized_Declaration_Struct {
        AssignExpr *assignment;
        VarDecl *declaration;
    };
    struct Switch_Statement_Core_Struct {
        Default *def;
        List<Case*> *case_list;
    };
    struct Statement_List_Struct
    {
      List<VarDecl*> *declarationList;
      List<Stmt*> *statementList;
    };
    struct Rest_Statement_Struct {
        Stmt *tb;
        Stmt *eb;
    };
    struct For_Rest_Statement_Struct {
        Expr *test;
        Expr *step;
    };



/* Line 1676 of yacc.c  */
#line 66 "y.tab.h"

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     T_Void = 258,
     T_Bool = 259,
     T_Int = 260,
     T_Float = 261,
     T_LessEqual = 262,
     T_GreaterEqual = 263,
     T_Equal = 264,
     T_NotEqual = 265,
     T_Dims = 266,
     T_And = 267,
     T_Or = 268,
     T_While = 269,
     T_For = 270,
     T_If = 271,
     T_Else = 272,
     T_Return = 273,
     T_Break = 274,
     T_Inc = 275,
     T_Dec = 276,
     T_Switch = 277,
     T_Case = 278,
     T_Default = 279,
     T_Continue = 280,
     T_Assign_Multiply = 281,
     T_Assign_Divide = 282,
     T_Assign_Add = 283,
     T_Assign_Subtract = 284,
     T_Vec2 = 285,
     T_Vec3 = 286,
     T_Vec4 = 287,
     T_Mat2 = 288,
     T_Mat3 = 289,
     T_Mat4 = 290,
     T_Identifier = 291,
     T_FieldSelect = 292,
     T_IntConstant = 293,
     T_FloatConstant = 294,
     T_BoolConstant = 295
   };
#endif
/* Tokens.  */
#define T_Void 258
#define T_Bool 259
#define T_Int 260
#define T_Float 261
#define T_LessEqual 262
#define T_GreaterEqual 263
#define T_Equal 264
#define T_NotEqual 265
#define T_Dims 266
#define T_And 267
#define T_Or 268
#define T_While 269
#define T_For 270
#define T_If 271
#define T_Else 272
#define T_Return 273
#define T_Break 274
#define T_Inc 275
#define T_Dec 276
#define T_Switch 277
#define T_Case 278
#define T_Default 279
#define T_Continue 280
#define T_Assign_Multiply 281
#define T_Assign_Divide 282
#define T_Assign_Add 283
#define T_Assign_Subtract 284
#define T_Vec2 285
#define T_Vec3 286
#define T_Vec4 287
#define T_Mat2 288
#define T_Mat3 289
#define T_Mat4 290
#define T_Identifier 291
#define T_FieldSelect 292
#define T_IntConstant 293
#define T_FloatConstant 294
#define T_BoolConstant 295




#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef union YYSTYPE
{

/* Line 1676 of yacc.c  */
#line 66 "parser.y"

    int integerConstant;
    bool boolConstant;
    char *stringConstant;
    float floatConstant;
    char identifier[MaxIdentLen+1]; // +1 for terminating null
    Identifier *id;
    IntConstant *intConstant;
    Decl *decl;
    List<Decl*> *declList;
    VarDecl *variableDecl;
    FnDecl *functionDecl;
    Type *type;
    Stmt *statement;
    Expr *expr;
    IfStmt *ifStatement;
    List<VarDecl*> *variableList;
    void *nothing;
    Operator *op;
    // Switch statement stuff
    LoopStmt *iterationStatement;
    Default *defa;
    SwitchStmt *switchStmt;
    Case *caseStmt;
    List<Case*> *caseStmtList;
    // Structs
    struct Rest_Statement_Struct rest_statement_struct;
    struct Statement_List_Struct statementList;
    struct Switch_Statement_Core_Struct switch_statement_core_struct;
    struct For_Rest_Statement_Struct for_rest_statement_struct;
    Initialized_Declaration_Struct initialized_declaration_struct;



/* Line 1676 of yacc.c  */
#line 198 "y.tab.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
#endif

extern YYSTYPE yylval;

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
} YYLTYPE;
# define yyltype YYLTYPE /* obsolescent; will be withdrawn */
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif

extern YYLTYPE yylloc;

